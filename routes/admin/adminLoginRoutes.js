var express = require('express');

var routes = function(){

    var adminRouter = express.Router();
    var adminLoginController = require('../../controllers/admin/adminLoginController')();

    adminRouter.route("/displayLoginPage")
        .get(adminLoginController.displayLoginPage)
        return adminRouter;
};

module.exports = routes;