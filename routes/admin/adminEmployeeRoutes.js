var express = require('express');

var routes = function(Employee){

    var adminRouter = express.Router();
    var adminEmployeeController = require('../../controllers/admin/adminEmployeeController')(Employee);

    adminRouter.route("/getEmployee")
        .get(adminEmployeeController.getEmployee)
        .post(adminEmployeeController.saveEmployee)

    return adminRouter;
};

module.exports = routes;