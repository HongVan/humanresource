var should = require('should'), // import should framework
    sinon = require('sinon'); // import simon framework

describe('Admin Controller Test',function(){

    describe('Get All Employees',function(){
        it('should not allow something',function(){
            var Employee = function(employee){this.save=function(){}};
            var req = {
              body : {
                  employeeName : "nguyen"
              }
            };

            var res = {
                status : sinon.spy(),
                send : sinon.spy()
            }
            var adminEmployeeController = require('../../controllers/admin/adminEmployeeController')(Employee);
            adminEmployeeController.saveEmployee(req,res);
            res.status.calledWith(200).should.equal(true,"Bad Status" + res.status.args[0][0]);
        })
    })
})