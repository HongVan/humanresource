var should = require('should'),
    request = require('supertest'),
    app = require('../../app.js'),
    mongoose = require('mongoose'),
    Employee = mongoose.model('Employee'),
    agent =  request.agent(app);

describe('CRUD Employee',function(){

    this.timeout(15000);

    it("should get employees from database",function(done){

        agent.get('/admin/employee/getEmployee')
            .expect(200)
            .end(function(err, res){
                if (err) throw err;
                done();
        });
    });

    it("should save employee in database",function(done){
        var employeeSave = {"employeeName" : "nguyen","jobTitle":"developer"};
        agent.post('/admin/employee/getEmployee')
            .send(employeeSave)
            .expect(200)
            .end(function(err,res){
                if (err) throw err;
                done();
            })
    });

    afterEach(function(done){
        Employee.remove().exec();
        done();
    })

});
