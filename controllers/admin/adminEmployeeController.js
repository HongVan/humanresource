var adminEmployeeController = function(Employee) {

    var getEmployee = function(req,res){
        Employee.find(function(err,employees){
            if(err) {
                console.log(err)
                res.status(500).send(err);
            } else {
                res.status(200);
                res.json(employees);
            }
        });
    };

    var saveEmployee = function(req,res){
        var employee = new Employee(req.body);
        employee.save();
        res.status(200);

        res.send(employee);
    }

    return {
        getEmployee : getEmployee,
        saveEmployee : saveEmployee
    }
}

module.exports = adminEmployeeController;