var config = {
    development: {
        //url to be used in link generation
        url: 'http://localhost:8000',
        //mongodb connection settings
        database: {
            host:   'mongodb://127.0.0.1',
            port:   '27017',
            db:     'humanresource'
        },
        //server details
        server: {
            host: 'localhost',
            port: '9000'
        }
    },
    test: {
        //url to be used in link generation
        url: 'http://localhost:8000',
        //mongodb connection settings
        database: {
            host:   'mongodb://127.0.0.1',
            port:   '27017',
            db:     'humanresourcetest'
        },
        //server details
        server: {
            host: 'localhost',
            port: '9001'
        }
    },
    production: {
        //url to be used in link generation
        url: 'http://my.site.com',
        //mongodb connection settings
        database: {
            host: 'mongodb://127.0.0.1',
            port: '27017',
            db:     'site'
        },
        //server details
        server: {
            host:   '127.0.0.1',
            port:   '3421'
        }
    }
};
module.exports = config;