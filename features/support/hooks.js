'use strict';

var driver = require('./world.js').getDriver();
var fs = require('fs');
var path = require('path');
var sanitize = require("sanitize-filename");
var myHooks = function () {

   /* this.Before(function (scenario) {

        var deletedFolder = process.cwd() + "/screenshots";

        if( fs.existsSync(deletedFolder) ) {
            fs.readdirSync(deletedFolder).forEach(function(file,index){
                var curPath = deletedFolder + "/" + file;
                if(fs.lstatSync(curPath).isDirectory()) { // recurse
                    deleteFolderRecursive(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(deletedFolder);
        }


    });*/

    this.After(function(scenario, callback) {
        //console.log("############ path is " + path.join("screenshot") );
        if(scenario.isFailed()) {
            this.driver.takeScreenshot().then(function(data){
                var base64Data = data.replace(/^data:image\/png;base64,/,"");
                fs.writeFile(path.join('screenshots', sanitize(scenario.getName() + ".png").replace(/ /g,"_")), base64Data, 'base64', function(err) {
                    if(err) console.log(err);
                });
            });
        }
        this.driver.manage().deleteAllCookies()
            .then(function() {
                callback();
            });
    });



    this.registerHandler('AfterFeatures', function (event, callback) {
        driver.quit();
        callback();
    });
};
module.exports = myHooks;


