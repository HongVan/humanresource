var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var gulpMocha = require('gulp-mocha');
var env = require('gulp-env'); // set enviroment
var supertest = require('supertest');
var concat = require('gulp-concat');// concat css files
var sync      = require('browser-sync').create()// reload the browser
var cucumber = require('gulp-cucumber');

var config = {
    paths : {
        css :[
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/bootstrap/dist/css/bootstrap-theme.min',
            'public/stylesheets/style.css'
        ],
        dist : './public/dist'
    }
}

// gulp.task('sync',function(){
//     notify : false,
//     serve : {
//         baseDir: '.'
//     }
// })

gulp.task('css',function(){
    gulp.src(config.paths.css)
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest(config.paths.dist + "/css"))

});

gulp.task('watch',function(){
    gulp.watch(config.paths.css,['css']);
    // gulp.watch(['*.html'], browserSync.reload);
    // gulp.watch(['js/*.js'], browserSync.reload);
    // gulp.watch(['css/*.css'], browserSync.reload);
});

gulp.task('default',['css','watch'],function(){
    sync.init(null, {
        files: ["public/**/*.*"],
        reloadDelay: 1000,
    });

    nodemon({
        script : 'app.js',
        ext : 'js,handlebars',

        ignore : ['./node_modules/**']
    })
    .on('restart',function(){
        sync.reload();
        console.log('Restarting ....')
    });
});



gulp.task('test',function(){
    gulp.src('tests/unitest/*.js',{read:false})
    .pipe(gulpMocha({reporter : 'nyan'}))

});

gulp.task('integration',function(){
    env({vars:{ENV :'test'}});
    gulp.src('tests/integration/*.js',{read:false})
        .pipe(gulpMocha({reporter : 'nyan'}))
});

gulp.task('cucumber', function() {
    return gulp.src('features/*').pipe(cucumber({
        'steps': 'features/steps/*.js',
        'format' : 'summary'
    }));
});