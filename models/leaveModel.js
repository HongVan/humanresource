var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var leaveModel = new Schema({
    leaveType : {
        type : String
    },
    fromDate : {
        type : Date
    },
    toDate : {
        type : Date
    },
    comment : {
        type : String
    }
});

module.exports = mongoose.model("Leave",leaveModel);