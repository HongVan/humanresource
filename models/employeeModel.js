var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var employeeModel = new Schema({
    employeeName : {
        type : String
    },
    jobTitle : {
        type : String
    }
});

module.exports = mongoose.model("Employee",employeeModel);