var express = require('express'); // import express
var mongoose = require('mongoose'); // import mongoose module
var bodyParser = require('body-parser'); // using body-parse to parse object from request to json
var app = express(); // using express framework
var morgan   = require('morgan');// use for logging

var env = process.env.ENV || 'development';
var config = require('./config/config')[env];

/*******************  Model ****************************************************************************************/
var Employee = require('./models/employeeModel');
var Leave = require('./models/leaveModel');
/*******************  End Model **************************************************************************************/


/*******************  ROUTING ****************************************************************************************/
var adminEmployeeRouter = require('./routes/admin/adminEmployeeRoutes')(Employee);
var adminLoginRouter = require('./routes/admin/adminLoginRoutes')();


app.use("/admin/employee",adminEmployeeRouter);
app.use("/admin/login",adminLoginRouter);
/********************* End ROUTING **********************************************************************************/




/******************** Configuration Handlebar *************************************************************************/

var handlebars = require('express3-handlebars' ).create({
  defaultLayout: 'main',
  helpers: {
    section: function(name, options){
      if(!this._sections) this._sections = {};
      this._sections[name] = options.fn(this);
      return null;
    },
    static: function(name) {
      return require('./lib/static.js').map(name);
    }
  }

});
app.engine('handlebars', handlebars.engine);
// set handle bar layout
app.set('view engine','handlebars');

/******************** End Configuration Handlebar ********************************************************************/


/******************** Using bodyParse to parse the object in body request to json object *****************************/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
/******************** End Configuration Handlebar ********************************************************************/

/******************** Configuration static url    ********************************************************************/

app.use(express.static(__dirname + '/public'));

/******************** End Configuration static url    ****************************************************************/


/******************** Handle common error page    ********************************************************************/
app.use(function(err,req, res, next){
  console. error(err. stack);
  res. status(404);
  res. render('404' );
});


app.use(function(err, req, res, next){
  console. error(err. stack);
  res. status(500);
  res. render('500' );
});

/******************** Configuration port *************************************************************************/
app.set('port',config.server.port);
app.listen(app. get('port' ), function(){
  console. log( 'App is running  http://localhost:' +
      app. get('port' ) + '; press Ctrl-C to terminate.' );
});
/******************** End Configuration port *************************************************************************/

/******************** Configuration MongoDB *************************************************************************/
var db  = mongoose.connect(config.database.host + ':' + config.database.port +'/'+config.database.db,function(error){
      if (error) {
        console.log(error);
      } else {
        console.log("Database connected ..." + config.database.db);
      }
});
/******************** End Configuration MongoDB **********************************************************************/


module.exports = app;
